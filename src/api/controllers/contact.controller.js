const httpStatus = require('http-status');
const { omit } = require('lodash');
const { Contact } = require('../models');
const { handler: errorHandler } = require('../middlewares/error');

/**
 * Load contact and append to req.
 * @public
 */
exports.load = async (req, res, next, id) => {
  try {
    const contact = await Contact.findById(id).where({ isDeleted: false });
    req.locals = { contact };
    return next();
  } catch (error) {
    return errorHandler(error, req, res);
  }
};

/**
 * Get contact
 * @public
 */
exports.get = (req, res) => res.json(req.locals.contact);

/**
 * Get current contact
 * @public
 */
exports.profile = (req, res) => res.json(req.contact);

/**
 * change password
 * @public
 */
exports.changePassword = async (req, res) => {
  const { oldPassword, newPassword } = req.body;
  const didMatch = await req.contact.passwordMatches(oldPassword);
  if (didMatch) {
    req.contact.password = newPassword;
    await req.contact.save();
    res.json(req.contact);
  } else {
    res.status(400).json({
      message: "Passwords don't match",
    });
  }
};

/**
 * Create new contact
 * @public
 */
exports.create = async (req, res, next) => {
  try {
    const contact = new Contact(req.body);
    const savedContact = await contact.save();
    res.status(httpStatus.CREATED);
    res.json(savedContact);
  } catch (error) {
    next(Contact.checkDuplicateEmail(error));
  }
};

/**
 * Update existing contact
 * @public
 */
exports.update = async (req, res, next) => {
  try {
    const ommitRole = req.locals.contact.role !== 'admin' ? 'role' : '';
    const updatedContact = omit(req.body, ommitRole);
    const contact = Object.assign(req.locals.contact, updatedContact);
    const savedContact = await contact.save();
    res.status(httpStatus.OK);
    res.json(savedContact);
  } catch (error) {
    next(error);
  }
};

/**
 * Update existing contact dashboardSettings
 * @public
 */
exports.updateDashboardSettings = async (req, res, next) => {
  try {
    const ommitRole = req.locals.contact.role !== 'admin' ? 'role' : '';
    const updatedContact = omit(req.body, ommitRole);
    const contact = Object.assign(req.locals.contact, updatedContact);
    const savedContact = await contact.save();
    res.status(httpStatus.OK);
    res.json(savedContact);
  } catch (error) {
    next(error);
  }
};

/**
 * Get contact list
 * @public
 */
exports.list = async (req, res, next) => {
  try {
    const contacts = await Contact.find(req.query);
    res.json(contacts);
  } catch (error) {
    next(error);
  }
};

/**
 * Delete contact
 * @public
 */
exports.remove = async (req, res, next) => {
  try {
    const { contact } = req.locals;
    contact.isDeleted = false;
    await contact.save();
    res.status(httpStatus.NO_CONTENT).end();
  } catch (error) {
    next(error);
  }
};
