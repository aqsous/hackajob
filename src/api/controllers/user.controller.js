const httpStatus = require('http-status');
const { omit } = require('lodash');
const { User } = require('../models');
const { handler: errorHandler } = require('../middlewares/error');

/**
 * Load user and append to req.
 * @public
 */
exports.load = async (req, res, next, id) => {
  try {
    const user = await User.findById(id).where({ isDeleted: false });
    req.locals = { user };
    return next();
  } catch (error) {
    return errorHandler(error, req, res);
  }
};

/**
 * Get user
 * @public
 */
exports.get = (req, res) => res.json(req.locals.user);

/**
 * Get current user
 * @public
 */
exports.profile = (req, res) => res.json(req.user);

/**
 * change password
 * @public
 */
exports.changePassword = async (req, res) => {
  const { oldPassword, newPassword } = req.body;
  const didMatch = await req.user.passwordMatches(oldPassword);
  if (didMatch) {
    req.user.password = newPassword;
    await req.user.save();
    res.json(req.user);
  } else {
    res.status(400).json({
      message: "Passwords don't match",
    });
  }
};

/**
 * Create new user
 * @public
 */
exports.create = async (req, res, next) => {
  try {
    const user = new User(req.body);
    const savedUser = await user.save();
    res.status(httpStatus.CREATED);
    res.json(savedUser);
  } catch (error) {
    next(User.checkDuplicateEmail(error));
  }
};

/**
 * Update existing user
 * @public
 */
exports.update = async (req, res, next) => {
  try {
    const ommitRole = req.locals.user.role !== 'admin' ? 'role' : '';
    const updatedUser = omit(req.body, ommitRole);
    const user = Object.assign(req.locals.user, updatedUser);
    const savedUser = await user.save();
    res.status(httpStatus.OK);
    res.json(savedUser);
  } catch (error) {
    next(error);
  }
};

/**
 * Update existing user dashboardSettings
 * @public
 */
exports.updateDashboardSettings = async (req, res, next) => {
  try {
    const ommitRole = req.locals.user.role !== 'admin' ? 'role' : '';
    const updatedUser = omit(req.body, ommitRole);
    const user = Object.assign(req.locals.user, updatedUser);
    const savedUser = await user.save();
    res.status(httpStatus.OK);
    res.json(savedUser);
  } catch (error) {
    next(error);
  }
};

/**
 * Get user list
 * @public
 */
exports.list = async (req, res, next) => {
  try {
    const users = await User.find(req.query);
    res.json(users);
  } catch (error) {
    next(error);
  }
};

/**
 * Delete user
 * @public
 */
exports.remove = async (req, res, next) => {
  try {
    const { user } = req.locals;
    user.isDeleted = false;
    await user.save();
    res.status(httpStatus.NO_CONTENT).end();
  } catch (error) {
    next(error);
  }
};
