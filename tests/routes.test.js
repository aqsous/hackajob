const request = require('supertest');
const app = require('../src/index');

describe('Post Endpoints', () => {
  it('should create a new contact', async () => {
    const res = await request(app)
      .post('/v1/contacts')
      .send({
        phoneNumber: '+962777888999',
        email: 'example@example.com',
        firstName: 'First Name',
        lastName: 'Last Name',
      });
    console.log('res', res);
    expect(res.statusCode).toEqual(401);
    // expect(res.body).toHaveProperty('post');
  });
  it('should get all contacts', async () => {
    const res = await request(app)
      .get('/v1/contacts').send();
    expect(res.statusCode).toEqual(401);
    // expect(res.body).toHaveProperty('post');
  });
});
