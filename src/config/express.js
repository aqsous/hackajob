const express = require('express');
const cors = require('cors');
const helmet = require('helmet');
const passport = require('passport');
const Sentry = require('@sentry/node');
const compress = require('compression');
const methodOverride = require('method-override');

const strategies = require('./passport');
const error = require('../api/middlewares/error');
const {
  dashboardUrl,
  env,
  sentryDsn,
} = require('../config/vars');
/**
 * Express instance
 * @public
 */

module.exports = () => {
  const app = express();

  Sentry.init({
    dsn: sentryDsn,
    environment: env,
  });

  // The request handler must be the first middleware on the app
  app.use(Sentry.Handlers.requestHandler());

  app.set('view engine', 'ejs');


  // parse body params and attache them to req.body
  app.use(express.json({ limit: '50mb' }));
  app.use(express.urlencoded({ extended: true, limit: '50mb' }));

  // gzip compression
  app.use(compress());

  // lets you use HTTP verbs such as PUT or DELETE
  // in places where the client doesn't support it
  app.use(methodOverride());

  // secure apps by setting various HTTP headers
  app.use(helmet());

  // enable CORS - Cross Origin Resource Sharing
  // app.use(cors());
  app.use(cors({
    origin: [
      dashboardUrl,
    ],
    optionsSuccessStatus: 200,
  }));

  // enable authentication
  app.use(passport.initialize());
  passport.use('jwt', strategies.jwt);

  app.use('/static', express.static('public'));

  // mount api v1 routes
  // eslint-disable-next-line global-require
  const routes = require('../api/routes/v1');
  app.use('/v1', routes);
  // app.use('/', chatbotRoutes);
  app.get('/', (req, res) => {
    res.json({});
  });
  // if error is not an instanceOf APIError, convert it.
  // app.use(error.converter);

  // catch 404 and forward to error handler
  app.use(error.notFound);

  // error handler, send stacktrace only during development
  app.use(error.handler);

  // The error handler must be before any other error middleware and after all controllers
  app.use(Sentry.Handlers.errorHandler());

  // module.exports = app;
  return app;
};
