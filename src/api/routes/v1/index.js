const express = require('express');

const router = express.Router();

router.use('/auth', require('./auth.route'));
router.use('/users', require('./user.route'));
router.use('/contacts', require('./contact.route'));

module.exports = router;
