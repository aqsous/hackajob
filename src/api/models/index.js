const Contact = require('./contact.model');
const User = require('./user.model');
const RefreshToken = require('./refreshToken.model');

module.exports = {
  Contact,
  User,
  RefreshToken,
};
