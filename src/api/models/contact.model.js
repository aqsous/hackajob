const mongoose = require('mongoose');


/**
 * Contact Schema
 * @private
 */
const contactSchema = new mongoose.Schema({
  email: {
    type: String,
    match: /^\S+@\S+\.\S+$/,
    trim: true,
    lowercase: true,
  },
  firstName: {
    type: String,
    maxlength: 128,
    trim: true,
  },
  lastName: {
    type: String,
    maxlength: 128,
    trim: true,
  },
  phoneNumber: {
    type: String,
    trim: true,
  },
  isDeleted: {
    type: Boolean,
    default: false,
  },
}, {
  collection: 'Contact',
  timestamps: true,
});

/**
 * @typedef Contact
 */
module.exports = mongoose.model('Contact', contactSchema);
