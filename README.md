
# Phone Book RESTful API

* [Getting Started](#getting-started)
* [Create Token](#create-token)
* [Phone Book](#phone-book)

## Getting Started
1- Install Node Modules
```
npm install
```
2- Run Application 
```
npm start
```

## Create Token
To create token you must have a user in this application 

### Create User
You can create new user by this request

```
curl --location --request POST 'localhost:1337/v1/auth/register' \
--data-raw '{
	"email": "<USER_EMAIL>",
	"firstName": "<FIRST_NAME>",
	"lastName": "<LAST_NAME>",
	"password": "<PASSWORD>"
}'
``` 


### Login User
If you have a user you can login using this request:

```
curl --location --request POST 'localhost:1337/v1/auth/login' \
--data-raw '{
	"email": "<USER_EMAIL>",
	"password": "<PASSWORD>"
}'
``` 


## Phone Book
### Create new contact
```
curl --location --request POST 'localhost:1337/v1/contacts' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer <TOKEN>' \
--data-raw '{
	"email": "<EMAIL>",
	"firstName": "<FIRST_NAME>",
	"lastName": "<LAST_NAME>",
	"phoneNumber": "<PHONE_NUMBER>"
}'
```

### Get all contacts
```
curl --location --request GET 'localhost:1337/v1/contacts' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer <TOKEN>' 
```

### Get contact by id
```
curl --location --request GET 'localhost:1337/v1/contacts/<CONTACT_ID>' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer <TOKEN>' 
```

### Update contact
```
curl --location --request PATCH 'localhost:1337/v1/contacts/<CONTACT_ID>' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer <TOKEN>' \
--data-raw '{
	"email": "<EMAIL>",
	"firstName": "<FIRST_NAME>",
	"lastName": "<LAST_NAME>",
	"phoneNumber": "<PHONE_NUMBER>"
}'
```

### Delete contact
```
curl --location --request DELETE 'localhost:1337/v1/contacts/<CONTACT_ID>' \
--header 'Content-Type: application/json' \
--header 'Authorization: Bearer <TOKEN>' 
```
